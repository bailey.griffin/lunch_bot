module Users
  class OmniauthCallbacksController < Devise::OmniauthCallbacksController
    def google_oauth2
      omniauth = request.env['omniauth.auth']
      @user = from_omniauth(omniauth)

      if @user.present? && @user.persisted?
        flash[:notice] = I18n.t(
          'devise.omniauth_callbacks.success',
          kind: 'Google'
        )
        sign_in_and_redirect @user, event: :authentication
      else
        flash[:alert] = 'User not valid.'
        session['devise.google_data'] = omniauth.except(:extra)
        Rails.logger.error("Omniauth - User not valid. #{omniauth.inspect}")
        redirect_to new_session_path
      end
    end

    def failure
      redirect_to new_session_path
    end

    private def from_omniauth(access_token)
      data = access_token.info
      email = data['email'].to_s

      if email.end_with?('@scimedsolutions.com')
        user = User.find_or_create_by!(email: email)
      end

      user
    end
  end
end
