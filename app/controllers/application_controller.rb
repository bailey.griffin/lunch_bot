# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  private def ensure_admin
    return if current_user.admin?

    flash[:error] = 'Unauthorized action. Please ask an admin for approval.'
    redirect_back fallback_location: root_path
  end
end
