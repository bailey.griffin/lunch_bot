# A user rating is a view-backed model that is used to weight the randomization
# of restaurant options for a new WTL (see weekly_training_lunch_bot.rb). This
# view is the cross product of `users` and `restaurants`. If a RestaurantRating
# exists for a user/restaurant combo, the `rating` column consists of that
# restaurant ratings `stars`. If no RestaurantRating exists for that
# user/restaurant combination, the `rating` column on the UserRating will be set
# to a default value (currently 3, the value midway between the min and max
# manual rating values).
class UserRating < ApplicationRecord
  belongs_to :restaurant_rating, optional: true
  belongs_to :restaurant
  belongs_to :user

  def readonly?
    true
  end
end
