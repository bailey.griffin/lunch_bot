class User < ApplicationRecord
  has_paper_trail
  devise :omniauthable, :timeoutable, :rememberable,
         omniauth_providers: [:google_oauth2]

  validates :email, presence: true, uniqueness: true

  has_many :restaurant_ratings, dependent: :destroy
  # View; no dependent option needed
  # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :user_ratings
  # rubocop:enable Rails/HasManyOrHasOneDependent
end
