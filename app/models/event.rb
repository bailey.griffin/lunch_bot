class Event < ApplicationRecord
  has_paper_trail

  validates :name, presence: true
  validates :occurs_on, presence: true, uniqueness: true

  belongs_to :restaurant, optional: true

  def self.next
    where(arel_table[:occurs_on].gt(Time.zone.today))
      .order(occurs_on: :asc)
      .first
  end

  def to_s
    name
  end
end
