class Restaurant < ApplicationRecord
  has_paper_trail

  validates :food_type, presence: true
  validates :menu_url, presence: true
  validates :menu_url, uri: true
  validates :name, presence: true, uniqueness: { case_sensitive: false }

  has_many :events, dependent: :restrict_with_error

  has_many :restaurant_ratings, dependent: :destroy
  # View; no dependent option needed
  # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :user_ratings
  # rubocop:enable Rails/HasManyOrHasOneDependent

  scope :recent_picks, -> do
    joins(:events)
      .order(occurs_on: :desc)
      .where(
        Event.arel_table[:occurs_on].lt(Time.zone.today).and(
          Event.arel_table[:occurs_on].gt(1.month.ago)
        )
      )
  end

  def to_s
    name
  end

  def rating_for(user)
    restaurant_ratings.find { |rating| rating.user_id == user.id }
  end
end
