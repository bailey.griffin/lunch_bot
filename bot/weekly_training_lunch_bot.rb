require 'slack-ruby-bot'

class WeeklyTrainingLunchBot < SlackRubyBot::Bot
  help do
    title 'Lunch Bot'
    desc 'This bot allows you to schedule new WTL orders.'

    command 'choose' do
      desc 'Choose a restaurant for the next WTL.'
      long_desc 'Choose a restaurant for the next event. ' \
                'Example: `choose Subway`.'
    end

    command 'new wtl' do
      desc 'Creates a WTL event and provides three restaurant options.'
      long_desc 'Creates a WTL event for the next Monday and provides three ' \
                'restaurant options that have not been used recently. ' \
                'Restaurants with the same type of food as last week will ' \
                'also be excluded.'
    end

    command 'random' do
      desc 'Provides three random restaurants.'
      long_desc 'Provides three restaurant options that have not been used ' \
                'recently. Restaurants with the same type of food as last ' \
                'week will also be excluded.'
    end
  end

  command 'hello' do |client, data, _match|
    client.say(channel: data.channel, text: 'Hello')
    sleep 1
    client.say(channel: data.channel, text: "It's me")
    sleep 1
    msg = "I was wondering if after all these years you'd like to eat :chompy:"
    client.say(channel: data.channel, text: msg)
  end

  command 'choose' do |client, data, match|
    name = match['expression'].to_s.strip.gsub('’', "'")
    restaurant = Restaurant.find_by(name: name)
    event = Event.next

    if restaurant.blank?
      message = "No restaurant with name #{name} found."
    elsif event
      event.update!(restaurant: restaurant)
      message = "*#{restaurant}* has been chosen for the event: " \
                "#{event} on #{event.occurs_on}. Please place your orders.\n" \
                "Menu: #{restaurant.menu_url}\nOrder doc: #{ENV['ORDER_DOC']}"
    else
      message = 'No upcoming events found'
    end

    client.say(channel: data.channel, text: message)
  end

  command 'new wtl' do |client, data, _match|
    event_attrs = {
      name: 'Weekly Training Lunch',
      occurs_on: Time.zone.today.next_occurring(:monday)
    }

    event = Event.find_by(event_attrs)

    if event
      message = "Next week's training lunch has already been created. " \
                "#{event.restaurant || 'No restaurant'} has been selected."
    else
      Event.create!(event_attrs)

      message = "Please vote for the restaurant for next week's WTL.\n"
      message += random_options
    end

    client.say(channel: data.channel, text: message)
  end

  command 'random' do |client, data, _match|
    client.say(channel: data.channel, text: random_options)
  end

  def self.random_options
    emoji = [':one:', ':two:', ':three:']

    random_restaurants.map.with_index do |restaurant, index|
      "#{emoji[index]} #{restaurant.name} #{restaurant.menu_url}"
    end.join("\n")
  end

  def self.random_restaurants
    weighter = OptionWeighter.new(restaurants_without_recent_picks)
    weighted_options = weighter.weight_options do |restaurant|
      UserRating.where(restaurant: restaurant).sum(:rating)
    end

    final_options = []
    final_options << weighted_options.sample until final_options.uniq.size == 3

    final_options.uniq
  end

  def self.restaurants_without_recent_picks
    restaurants = Restaurant.all
    recent_picks = Restaurant.recent_picks
    restaurants -= recent_picks
    last_food_type = recent_picks.first.food_type

    if last_food_type.present?
      restaurants.reject! do |restaurant|
        restaurant.food_type.to_s.downcase == last_food_type.downcase
      end
    end

    restaurants
  end
end
