class CreateRestaurants < ActiveRecord::Migration[5.2]
  def change
    create_table :restaurants do |t|
      t.string :name, null: false
      t.string :menu_url, null: false
      t.string :food_type, null: false

      t.timestamps
    end
  end
end
