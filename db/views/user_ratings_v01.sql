SELECT restaurant_ratings.id,
       COALESCE(restaurant_ratings.stars, 3) AS rating,
       restaurants.id AS restaurant_id,
       users.id AS user_id

FROM users

CROSS JOIN restaurants
LEFT  JOIN restaurant_ratings
        ON restaurant_ratings.user_id = users.id
          AND restaurant_ratings.restaurant_id = restaurants.id
