# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'
require_relative '../bot/weekly_training_lunch_bot'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module LunchBot
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    %w[services validators].each do |directory|
      config.autoload_paths += Dir["#{config.root}/app/#{directory}"]
    end
  end
end
